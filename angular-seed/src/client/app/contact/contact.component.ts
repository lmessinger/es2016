import { Component, OnInit,Input,Output ,EventEmitter} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  moduleId: module.id, // for templateUrl resolution
  selector: 'app-contact',
  templateUrl: 'contact.component.html',
  styleUrls: ['contact-component.css']
})
export class ContactComponent implements OnInit {
  contactForm: FormGroup;
  @Input() firstName:string;
  @Output() childEvent = new EventEmitter();

  constructor(private formBuilder: FormBuilder) {}

  ngOnInit() {
    this.contactForm = this.formBuilder.group({
      name: ['', Validators.required],
      email: ['', [Validators.required]],
      content: ['', [Validators.required, Validators.minLength(10)]]
    });
  }
  submitForm(): void {
    console.log(this.contactForm);
  }

  myClick(ev:any):void {
    console.log(ev);
    
    this.childEvent.next(ev);
  }
}
