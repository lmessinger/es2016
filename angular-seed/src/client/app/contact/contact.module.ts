import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ContactComponent } from './contact.component';

import {ReactiveFormsModule, FormsModule } from '@angular/forms';

@NgModule({
  imports: [CommonModule,FormsModule,ReactiveFormsModule],
  declarations: [ContactComponent],
  exports: [ContactComponent] // so importers of us could use our <app-contact>
})
export class ContactModule { }
