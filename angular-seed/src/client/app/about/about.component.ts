import { Component, OnInit } from '@angular/core';
import { NameListService } from '../shared/index';

/**
 * This class represents the lazy loaded AboutComponent.
 */
@Component({
  moduleId: module.id,// for templateUrl resolution
  selector: 'sd-about',
  templateUrl: 'about.component.html',
  styleUrls: ['about.component.css']
})
export class AboutComponent  implements OnInit {

  /**
   * 
   */
  constructor(public nameListService: NameListService) {}


  newestName: string = '';
  errorMessage: string;
  names: any[] = [];
  
  /**
   * 
   */
  ngOnInit() {
    this.getLastAddedName();

  }

  /**
   * Handle the nameListService observable
   */
  getLastAddedName() {
    this.nameListService.get()
      .subscribe(
        names => {
          this.names = names;
          this.newestName = names[names.length-1] 
        } ,
        error => this.errorMessage = <any>error
      );
  }

  myHandler(childEvent:any) {
    console.log(childEvent);
  }
}
