import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AboutComponent } from './about.component';
import { AboutRoutingModule } from './about-routing.module';
import { ContactModule } from '../contact/contact.module';

@NgModule({
  imports: [CommonModule, AboutRoutingModule,ContactModule],
  declarations: [AboutComponent],
  exports: [AboutComponent]
})
export class AboutModule { }
