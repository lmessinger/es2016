import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { APP_BASE_HREF } from '@angular/common';
import { HttpModule } from '@angular/http';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';

import { AboutModule } from './about/about.module';
import { HomeModule } from './home/home.module';
import { SharedModule } from './shared/shared.module';

/**
 * NgModule is a decorator to make a class, a module class
 * This here defines a ROOT module. main.ts imports it and will bootstrap from it
 * 
 * imports — the BrowserModule that this and every application needs to run in a browser.
 * ** Only NgModule classes go in the imports array. Don't put any other kind of class in imports.    ***
 * 
 * declarations — the application's lone component, which is also ...
 * *** Only declarables — components, directives and pipes — belong in the declarations array.        *** 
 * *** Don't put any other kind of class in declarations; not NgModule classes, not service classes,  *** 
 * *** not model classes. *** *
 * 
 * bootstrap — the root component that Angular creates and inserts into the index.html host web page. ***
 * *** the bootstrapping process creates the component(s) listed in the bootstrap array and inserts   ***
 * *** each one into the browser DOM. ***
 * *** Each bootstrapped component is the base of its own tree of components. Inserting a bootstrapped *** 
 * *** component usually triggers a cascade of component creations that fill out that tree.           ***
 * *** You can call the one root component anything you want but most developers call it AppComponent.***
 */
@NgModule({
  imports: [BrowserModule, HttpModule, AppRoutingModule, AboutModule, HomeModule, SharedModule.forRoot()],
  declarations: [AppComponent],
  providers: [{
    provide: APP_BASE_HREF,
    useValue: '<%= APP_BASE %>'
  }],
  bootstrap: [AppComponent]

})
export class AppModule { }
