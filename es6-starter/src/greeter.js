export class Greeter {
  constructor(message) {
    this.message = message;
  }
  greet() {
    var element = document.querySelector('#content');
    element.innerHTML = this.message;
  }
};

// Convenience function for logging. Look funny? It won't by the end of this.
let _ = msg => console.log(msg);

/*
 * let is the new var
 */
 _('1. LET IS THE NEW VAR')
let someVariable = 'some value';
_(someVariable); // some value

// Value is mutable
_('VALUE IS MUTABLE')
someVariable = 'another value';
_(someVariable); // another value

// This would cause an error, cannot redeclare in the same block/scope.
// let someVariable = 'yet another value';
_('ERROR: let someVariable = \'yet another value\'')

// This is OK, as it's a different block/scope.
function something() {
  let someVariable = 'yet another value';
  // do stuff
}

/*
 * Constants
 * Cannot be changed once they are set.
 */
 _('2. CONST: const a = \'Some string\'')
const a = 'Some string';
// The following line would generate an error. 'a' is read only.
// a = 'Another string';

/*
 * Template strings
 */
 _('3. TEMPLATE STRINGS')
let firstName = 'Brad';
let lastName = 'Daily';

_(`My name is ${firstName} ${lastName}.`); // My name is Brad Daily.

// Can be multiline too
let multilineString = `My name is ${firstName} ${lastName}.
Really nice to meet ya.`;

_(multilineString); // My name is Brad Daily.\nReally nice to meet ya.

/*
 * 'Spread' operator
 * Creates an array from all passed arguments
 */
 _('4. SPREAD')
let spread = function(...x) {
  return x;
}

_(spread(1, 2, 3)); // [1, 2, 3]

/*
 * Fat arrow function syntax
 */
_('5. FAT ARROW FUNCTION');
/* Old way

  var helloWorld = function() {
    return 'Hello world';
  }
*/

let helloWorld = () => 'Hello world.';

_(helloWorld()); // Hello world.

// Here's that spread function from earlier
let newSpread = (...x) => x;

_(newSpread(3, 4, 5)); // [3, 4, 5]

// Example using spread operator plus arrow functions.
// add()  will sum any numbers passed to it as arguments
// The reduce() method applies a function against an accumulator and
// each value of the array (from left-to-right) to reduce it to a single value..
let add = (...x) => x.reduce((previous, current) => previous + current);
_(add(1, 5, 10, 20)); // 36

// Array sort
_([1, 5, 8, 2, 3, 4].sort((a, b) => a < b ? -1 : 1)); // [1, 2, 3, 4, 5, 8]

// the nice thing: Lexical this: Arrow functions inherit the scope they are defined in.
// No more var self = this;
// See: this.owner in the sayFruits function
let pantry = {
  owner: 'Jim',
  fruits: ['apple', 'orange', 'pear'],
  sayFruits() {
    this.fruits.forEach(fruit => {
      _(`${this.owner}'s pantry has ${fruit}s. see ES5 source!`);
    });
  }
}

pantry.sayFruits();
// Jim's pantry has apples
// Jim's pantry has oranges
// Jim's pantry has pears

/*
 * Object literals
 */
let x = 50;
let y = 100;
_('6. OBJECT LITERALS');
/* Old way

  var coordinates = {
    x: x,
    y: y
  }

*/

let coordinates = {
  x, y
};

_(coordinates); // Object { x: 50, y: 100}

/*
 * Destructuring
 */
 _('7. DESTRUCTURING');
let http = () => [404, 'Not found'];
let [status, textResponse] = http();

_(status); // 404
_(textResponse); // Not found

// list matching
var [a1, , b1] = [1,2,3];
_([a1,b1]); // 1,3
/*
 * Classes
 */
 _('8. CLASSSSSSSSSSES');
class Person {
  
  constructor(firstName, lastName, age) {
    this.firstName = firstName;
    this.lastName = lastName;
    this.age = age;
  }

  // Getters allow for dynamic properties
  // In this example, you can use .fullName instead of .fullName()
  // See the greeting() function below
  get fullName() {
    return `${this.firstName} ${this.lastName}`;
  }

  greeting() {
    return `Hi, my name is ${this.fullName} and I am ${this.age} years old.`;
  }

  isAdult() {
    return this.age >= 18;
  }
}

let me = new Person('Brad', 'Daily', 35);
_(me.isAdult()); // true
_(me.greeting()); // Hi, my name is Brad Daily and I am 35 years old.
me.age = 36;
_(me.greeting()); // Hi, my name is Brad Daily and I am 36 years old.

// Class extension
class Pirate extends Person {
  greeting() {
    return `Arggh, me name is ${this.fullName} and I've been sailing these here seas for ${this.age} years.`;
  }
}

var pirate = new Pirate('Brad', 'Daily', 35);
_(pirate.greeting()); // Arggh, me name is Brad Daily and I've been sailing these here seas for 35 years.

// class super
class Singer extends Person {
  constructor() {
    super('Yehoram','Gaon','many, many');
  }
  greeting() {
	  
    return `Arggh, me name is ${this.fullName} and I've been here ${this.age} years.`;
  }
}
let singer = new Singer();
_(singer.greeting());


/*
 * Default parameters values
 */
 _('9. DEFAULT PARAMETERS VALUES');
function repeat(toldYa = 'Nothin') {
  return `You told me ${toldYa}.`;
}

_(repeat()); // You told me Nothin'.
_(repeat("Somethin'")); // You told me Somethin'.

// Arrow version
let repeat2 = (toldYa = "Nothin'") => `You told me ${toldYa}.`;

_(repeat2()); // You told me Nothin'.
_(repeat2("Somethin'")); // You told me Somethin'.

/*
 * Sets
 * A Set is a collection of unique elements
 */
 _('10. SETS');
let letters = new Set();
letters.add('A');
_(letters.size); // 1

// Ignores duplicate values
letters.add('A');
_(letters.size); // 1

_(letters.has('A')); // true
_(letters.has('B')); // false

letters.add('B');

for (let letter of letters) _(letter); // Outputs A, then B

_(letters.delete('Z')); // returns false since Z is was not in letters before delete()
_(letters.delete('A')); // returns true since A is was in letters before delete()

letters.clear();
_(letters.size); // 0

/*
 * Maps
 * The keys of a Map can be arbitrary values
 */
 _('11. MAPS');
let map = new Map();
map.set('string-key', 'I belong to the string-key');

// Keys can be anything, like a DOM element
map.set(document.getElementsByTagName('h1')[0], 'I belong to the first <h1> in the DOM.');

_(map.size); // 2

_(map.get(document.getElementsByTagName('h1')[0])); // I belong to the first <h1> in the DOM.

for (let [key, val] of map.entries()) {
  _(key); // Outputs 'string-key', then the <h1>
  _(val); // Outputs 'I belong to the string-key', then 'I belong to the first <h1> in the DOM.'
}
/*
* WeakMaps
* WeakMap is a Map that doesn’t prevent its keys from being garbage-collected.
* That means that you can associate private data with objects without having to worry about memory leaks:
*/
_('12. WEAK MAP/SET');
let _counter = new WeakMap();
let _action = new WeakMap();
class Countdown {
    constructor(counter, action) {
        _counter.set(this, counter);
        _action.set(this, action);
        this.x = 12;
    }
    dec() {
        // the counter will receive a number even if this was garbage-collected
        let counter = _counter.get(this);
        console.log('counter1',_counter,counter);
        if (counter < 1) return;
        counter--;
        _counter.set(this, counter);
        // when reach 0, fire the action
        if (counter === 0) {
            _action.get(this)();
        }
    }
}

_('12a. COMPUTED METHOD NAMES');
/*
* define the name of a method via an expression, if you put it in square brackets.
*/
const mamashStam = 'myMethod called';
class Stam {
    myMethod() {
      _(mamashStam);
    }
}

const m = 'myMethod' + '12';
class Stamy {
    [m]() {
        _(mamashStam);
    }
}
let stam = new Stam();
let stamy = new Stamy();
_(stam.myMethod()===stamy.myMethod12());
_(stamy.myMethod12());

_('14. ITERATORS + FOR..OF');
// if an object has Symbol.iterator named function, it is iteratable by for..of
let fibonacci = {
  [Symbol.iterator]() {
    let pre = 0, cur = 1;
    return {
      next() {
        [pre, cur] = [cur, pre + cur];
        return { done: false, value: cur }
      }
    }
  }
}

for (var n of fibonacci) {
  // truncate the sequence at 1000
  if (n > 10)
    break;
  _(n);
}

_('15. GENERATORS')
// Generators simplify iterator-authoring using function* and yield.
// They include the next() within them
// var fibonacci_star = {
//   [Symbol.iterator]: function*() {
//     var pre = 0, cur = 1;
//     for (;;) {
//       var temp = pre;
//       pre = cur;
//       cur += temp;
//       yield cur;
//     }
//   }
// }
//
// for (var n of fibonacci_star) {
//   // truncate the sequence at 1000
//   if (n > 10)
//     break;
//   _(n);
// }

_('16. UNICODE')
// More Unicode support
// like es5
let shin = "ש";
_('ש');
// new
let shinbet ='\u{05E9}';
_(shinbet);
_(shin.length==shinbet.length)

_('17. PROXIES')
//Meta programming
// intercepting calls on object
// unsupported by Babel
let target = {};
let handler = {
    /** Intercepts: getting properties */
    get(target, propKey, receiver) {
        _(`GET ${propKey}`);
        return 123;
    },

    /** Intercepts: checking whether properties exist */
    has(target, propKey) {
        _(`HAS ${propKey}`);
        return true;
    }
};
//let proxy = new Proxy(target, handler);
//proxy.stam(); // GET stam
// (function() {
//   'shalom' in proxy; // HAS shalom
// }());

_('18. SYMBOLS')
// a key does not have to be a string
// global scoped symbol
var key = Symbol("mykey");

function MyClass(privateData) {
  this[key] = privateData;
}

MyClass.prototype = {
  doStuff: function() {
    _(this[key])
  }
};


_(typeof key === "symbol");
// Limited support from Babel, full support requires native implementation.
var c = new MyClass("hello")
c.doStuff();
_(c["mykey"] === undefined);
_(c[key] === "hello");


_('19. Math + Number + String + Object APIs')
// Many new library additions, including core Math libraries,
// Array conversion helpers, and Object.assign for copying.

/*unsupported by Babel _(Number.EPSILON)
_(Number.isInteger(Math.Infinity) // false
_(Number.isNaN("NaN")) // false
*/
_(Math.acosh(3)) // 1.762747174039086
_(Math.hypot(3, 4)) // 5
_(Math.imul(Math.pow(2, 32) - 1, Math.pow(2, 32) - 2)) // 2

_("abcde".includes("cd")) // true
_("abc".repeat(3)) // "abcabcabc"

_(Array.from(document.querySelectorAll("*"))) // Returns a real Array
_(Array.of(1, 2, 3)) // Similar to new Array(...), but without special one-arg behavior
_([0, 0, 0].fill(7, 1)) // [0,7,7]
_([1,2,3].findIndex(x => x == 2)) // 1
_(["a", "b", "c"].entries()) // iterator [0, "a"], [1,"b"], [2,"c"]
_(["a", "b", "c"].keys()) // iterator 0, 1, 2
///_(["a", "b", "c"].values()) // iterator "a", "b", "c"  unsupported by Babel
class Point {
constructor(x,y) {
  [this.x,this.y] = [x,y];
}
}
let point = new Point(9,8);
Object.assign(point, { origin: new Point(0,0) })
_(point) //Point {x: 9, y: 8, origin: 0,0}


_('20. PROMISES')
function timeout(duration = 0) {
    return new Promise((resolve, reject) => {
      // call resolve after duractiom
        setTimeout(resolve, duration);
    })
}

var p = timeout(1000).then(() => {
    _('resolved called')
    return timeout(2000); // create another promise
} ,() => {
  _('reject called')
    throw new Error("hmm");
})
.then(()=>{_('second then  resolved');return 12;})
.then(() =>{_('third chained')});
