var gulp = require('gulp');
var connect = require('gulp-connect');

gulp.task('styles', function() {
  gulp.src(config.src)
    .pipe(gulp.dest(config.dest))
    .pipe(connect.reload());
});
