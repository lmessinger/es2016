import React from 'react';
import TodoStore from '../stores/TodoStore';
import ActionCreator from '../actions/TodoActionCreators';
import App from './App.jsx';

export default React.createClass({
  _onChange() {
    // (LM==> 9=) when a change event is received
    //        we take all data again
    //        this will cause a re-render (10)
    this.setState(TodoStore.getAll());
  },

  getInitialState() {
    return TodoStore.getAll();
  },

  componentDidMount() {
    //(LM==> 4.2=) App subscribe to the store on creation
    TodoStore.addChangeListener(this._onChange);
  },

  componentWillUnmount() {
    TodoStore.removeChangeListener(this._onChange);
  },

  handleAddTask(e) {
    // (LM==> 5.2=) data flow: a click on a button calls the ActionCreator
    let title = prompt('Enter task title:');
    if (title) {
      ActionCreator.addItem(title);
    }
  },

  handleClear(e) {
    ActionCreator.clearList();
  },

  render() {
    let {tasks} = this.state;
    // (LM==> 3=)
    //        the App is the top-most component.
    //        we pass properties of the state (tasks)
    //        which receives data INTO the App
    //        and the handlers of events coming FROM the App
    return (
      <App
        onAddTask={this.handleAddTask}
        onClear={this.handleClear}
        tasks={tasks} />
    );
  }
});
